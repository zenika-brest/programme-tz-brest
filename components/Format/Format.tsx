import { Text } from '@chakra-ui/react';
import { FC } from 'react';
import { FormatFragment } from '~generated/operations';

type FormatProps = FormatFragment;

const Format: FC<FormatProps> = ({ length }) => {
  return <Text>🕑 {length}</Text>;
};

export default Format;

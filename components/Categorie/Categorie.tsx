import { Text } from '@chakra-ui/react';
import { FC } from 'react';
import { CategorieFragment } from '~generated/operations';

type CategorieProps = CategorieFragment;

const Categorie: FC<CategorieProps> = ({ name }) => <Text>{name}</Text>;

export default Categorie;

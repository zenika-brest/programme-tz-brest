import {
  Box,
  Button,
  Flex,
  LayoutProps,
  LinkBox,
  LinkOverlay,
  List,
  ListItem,
  ModalHeader,
  UseDisclosureReturn
} from '@chakra-ui/react';
import { FC } from 'react';
import ReactMarkdown from 'react-markdown';
import { Categorie } from '~components/Categorie';
import { Format } from '~components/Format';
import { Speaker } from '~components/Speaker';
import { TalkFragment } from '~generated/operations';

type TalkProps = TalkFragment &
  Pick<LayoutProps, 'maxW'> &
  Partial<UseDisclosureReturn> & {
    isAbstract?: boolean;
  };

const Talk: FC<TalkProps> = ({
  title,
  speakers,
  categorie,
  abstract,
  format,
  onOpen,
  maxW,
  isAbstract = false
}) => {
  return (
    <LinkBox as={Flex} direction="column" minW="sm" maxW={maxW}>
      {onOpen ? (
        <Flex>
          <LinkOverlay
            as={Button}
            variant="link"
            onClick={onOpen}
            mb={2}
            noOfLines={1}
          >
            {title}
          </LinkOverlay>
        </Flex>
      ) : (
        <ModalHeader as={Flex} justifyContent="center">
          {title}
        </ModalHeader>
      )}

      {isAbstract && (
        <Box as={ReactMarkdown} mb={2}>
          {abstract}
        </Box>
      )}
      <Flex alignItems="center" justifyContent="space-between">
        <List spacing={1}>
          {speakers.map((speaker) => (
            <ListItem as={Speaker} key={speaker.id} {...speaker} />
          ))}
        </List>
        <List spacing={1}>
          {categorie && <ListItem as={Categorie} {...categorie} />}
          {format && <ListItem as={Format} {...format} />}
        </List>
      </Flex>
    </LinkBox>
  );
};

export default Talk;

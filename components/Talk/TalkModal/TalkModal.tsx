import {
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalOverlay,
  UseDisclosureReturn
} from '@chakra-ui/react';
import { FC } from 'react';
import { Talk } from '~components/Talk';
import { TalkFragment } from '~generated/operations';

type TalkModalProps = TalkFragment &
  Pick<UseDisclosureReturn, 'isOpen' | 'onClose'>;

const TalkModal: FC<TalkModalProps> = ({ isOpen, onClose, ...talk }) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose} size="2xl">
      <ModalOverlay />
      <ModalContent>
        <ModalBody>
          <Talk {...talk} isAbstract />
        </ModalBody>
        <ModalCloseButton />

        <ModalFooter>
          <Button colorScheme="blue" mr={3} onClick={onClose} size="sm">
            Fermer
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default TalkModal;

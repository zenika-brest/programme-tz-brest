import {
  Center,
  Table as CTable,
  TableCaption,
  TableContainer,
  Tbody,
  Td,
  Tfoot,
  Thead,
  Tr
} from '@chakra-ui/react';
import { FC, ReactElement } from 'react';
import {
  EmptySlotFragment,
  KeynoteFragment,
  MealFragment,
  PauseFragment,
  ProgrammeTableQuery,
  SecretFragment,
  TalkFragment,
  WelcomFragment
} from '~generated/operations';
import { ColumsHeaders } from './ColumsHeaders';
import { SecretColumn } from './SecretColumn';
import { TalkColumn } from './TalkColumn';

type TableProps = {
  data: ProgrammeTableQuery;
};

const Table: FC<TableProps> = ({
  data: {
    event: { schedules, tracks }
  }
}) => {
  return (
    <TableContainer>
      <CTable>
        <Thead>
          <ColumsHeaders tracks={tracks} />
        </Thead>
        <Tbody>
          {schedules.map(({ id, label, slots }) => (
            <Tr key={id}>
              <Td>{label}</Td>
              {slots.map((slot) =>
                formatSlot(slot, slots.length === tracks.length)
              )}
            </Tr>
          ))}
        </Tbody>
        <Tfoot>
          <ColumsHeaders tracks={tracks} />
        </Tfoot>
        <TableCaption>Organisé par Zenika Brest</TableCaption>
      </CTable>
    </TableContainer>
  );
};

type SlotFragment =
  | WelcomFragment
  | TalkFragment
  | MealFragment
  | PauseFragment
  | EmptySlotFragment
  | KeynoteFragment
  | SecretFragment;

const formatSlot = (
  slot: SlotFragment,
  tracksCompeted: boolean
): ReactElement => {
  switch (slot.__typename) {
    case 'Secret':
      return (
        <Td key={slot.id} as="td" colSpan={2}>
          <Center>
            <Center as="h3">
              <SecretColumn {...slot} />
            </Center>
          </Center>
        </Td>
      );
    case 'EmptySlot':
      return (
        <Td key={slot.id}>
          <Center as="h3">{slot.title}</Center>
        </Td>
      );
    case 'Talk':
      return tracksCompeted ? (
        <Td key={slot.id}>
          <TalkColumn {...slot} tracksCompeted />
        </Td>
      ) : (
        <Td key={slot.id} as="td" colSpan={2}>
          <Center>
            <TalkColumn {...slot} />
          </Center>
        </Td>
      );
    case 'Meal':
    case 'Pause':
      return (
        <Td
          as="td"
          key={slot.id}
          colSpan={2}
          bgColor="gray.100"
          _dark={{ bgColor: 'gray.700' }}
        >
          <Center as="h3">{slot.title}</Center>
        </Td>
      );
    default:
      return (
        <Td key={slot.id} as="td" colSpan={2}>
          <Center as="h3">{slot.title}</Center>
        </Td>
      );
  }
};

export default Table;

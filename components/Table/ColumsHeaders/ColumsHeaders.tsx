import { Center, Th, Tr } from '@chakra-ui/react';
import { FC } from 'react';
import { TrackFragment } from '~generated/operations';

type ColumsProps = { tracks: TrackFragment[] };

export const ColumsHeaders: FC<ColumsProps> = ({ tracks }) => (
  <Tr>
    <Th>
      <Center>horaire</Center>
    </Th>
    {tracks.map(({ id, name }, index) => (
      <Th key={id}>
        <Center>
          Track {++index} - {name}
        </Center>
      </Th>
    ))}
  </Tr>
);

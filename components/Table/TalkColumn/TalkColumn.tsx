import { useDisclosure } from '@chakra-ui/react';
import { FC } from 'react';
import { Talk, TalkModal } from '~components/Talk';
import { TalkFragment } from '~generated/operations';

type ColumnProps = TalkFragment & { tracksCompeted?: boolean };

export const TalkColumn: FC<ColumnProps> = ({ tracksCompeted, ...talk }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <>
      <Talk {...talk} onOpen={onOpen} maxW={tracksCompeted ? 'sm' : 'xl'} />
      <TalkModal {...talk} isOpen={isOpen} onClose={onClose} />
    </>
  );
};

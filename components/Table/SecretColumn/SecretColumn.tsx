import { useDisclosure } from '@chakra-ui/react';
import { FC } from 'react';
import { Secret, SecretModal } from '~components/Secret';
import { SecretFragment } from '~generated/operations';

type SecretProps = SecretFragment & { tracksCompeted?: boolean };

export const SecretColumn: FC<SecretProps> = ({
  tracksCompeted,
  ...secret
}) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <>
      <Secret {...secret} onOpen={onOpen} maxW={tracksCompeted ? 'sm' : 'xl'} />
      <SecretModal {...secret} isOpen={isOpen} onClose={onClose} />
    </>
  );
};

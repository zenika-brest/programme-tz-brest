import {
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalOverlay,
  UseDisclosureReturn
} from '@chakra-ui/react';
import { FC } from 'react';
import Secret from '~components/Secret/Secret';
import { SecretFragment } from '~generated/operations';

type SecretModalProps = SecretFragment &
  Pick<UseDisclosureReturn, 'isOpen' | 'onClose'>;

const SecretModal: FC<SecretModalProps> = ({ isOpen, onClose, ...secret }) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose} size="2xl">
      <ModalOverlay />
      <ModalContent>
        <ModalBody>
          <Secret {...secret} isAbstract />
        </ModalBody>
        <ModalCloseButton />

        <ModalFooter>
          <Button colorScheme="blue" mr={3} onClick={onClose} size="sm">
            Fermer
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default SecretModal;

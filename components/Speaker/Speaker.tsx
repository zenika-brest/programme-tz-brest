import { Avatar, Flex, Text } from '@chakra-ui/react';
import { FC } from 'react';
import { SpeakerFragment } from '~generated/operations';

type SpeakerProps = SpeakerFragment;

const Speaker: FC<SpeakerProps> = ({ avatar, name }) => {
  return (
    <Flex alignItems="center" justifyContent="flex-start">
      <Avatar size="xs" name={name} src={avatar} mr={2} />
      <Text>{name}</Text>
    </Flex>
  );
};

export default Speaker;

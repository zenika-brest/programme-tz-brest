import { fetchEvent } from '~pages/api/events/[id]';

const context = async () => {
  const id = process.env.ENVT_ID;
  if (!id) throw Error('process.env.ENVT_ID not found');
  return {
    id,
    event: await fetchEvent(id)
  };
};

export default context;

import SchemaBuilder from '@pothos/core';
import { IEvent } from './types';

const builder = new SchemaBuilder<{
  Context: {
    id: string;
    event: IEvent;
  };
}>({});

builder.queryType();

export default builder;

import builder from '../builder';
import { EmptySlot } from './EmptySlot';
import { Keynote } from './Keynote';
import { Meal } from './Meal';
import { Pause } from './Pause';
import { Secret } from './Secret';
import { Talk } from './Talk';
import { Welcom } from './Welcom';

export const Slot = builder.unionType('Slot', {
  types: [Welcom, Keynote, Talk, Pause, Meal, EmptySlot, Secret],
  resolveType: ({ type }) => {
    switch (type) {
      case 'WELCOM':
        return Welcom;
      case 'KEYNOTE':
        return Keynote;
      case 'TALK':
        return Talk;
      case 'PAUSE':
        return Pause;
      case 'MEAL':
        return Meal;
      case 'EMPTY':
        return EmptySlot;
      case 'SECRET':
        return Secret;
    }
  }
});

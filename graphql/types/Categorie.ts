import { ICategorie } from '.';
import builder from '../builder';

export const Categorie = builder.objectRef<ICategorie>('Categorie');

Categorie.implement({
  fields: (t) => ({
    id: t.exposeID('id'),
    name: t.exposeString('name'),
    description: t.exposeString('description')
  })
});

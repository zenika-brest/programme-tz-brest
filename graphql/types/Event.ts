import { IEvent, IPartialTalk, ISchedule, ITalk } from '.';
import builder from '../builder';
import { schedules } from '../data/schedules';
import { tracks } from '../data/tracks';
import { Schedule } from './Schedule';
import { Talk } from './Talk';
import { Track } from './Track';

export const Event = builder.objectRef<IEvent>('Event');

Event.implement({
  fields: (t) => ({
    id: t.field({
      type: 'ID',
      resolve: (_parent, _args, { id }) => id
    }),
    name: t.exposeString('name'),
    tracks: t.field({
      type: [Track],
      resolve: () => tracks
    }),
    schedules: t.field({
      type: [Schedule],
      resolve: (_parent, _args, { event: { talks } }) => map(talks)
    }),
    talks: t.field({
      type: [Talk],
      resolve: (_parent, _args, { event: { talks } }) => talks
    })
  })
});

builder.queryField('event', (t) => {
  return t.field({
    type: Event,
    resolve: (_root, _args, { event }) => event
  });
});

const map = (talks: ITalk[]): ISchedule[] =>
  schedules.map(({ slots, ...schedule }) => ({
    slots: slots.map((slot) => {
      switch (slot.type) {
        case 'TALK':
          return findTalk(slot, talks);
        default:
          return slot;
      }
    }),
    ...schedule
  }));

const findTalk = (slot: IPartialTalk, talks: ITalk[]): ITalk => {
  const talk = talks.find((talk) => talk.id === slot.id);
  if (!talk) throw new Error(`talk with id ${slot.id} does not exist`);
  return { ...talk, ...slot };
};

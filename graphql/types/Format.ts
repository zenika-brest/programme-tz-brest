import { IFormat } from '.';
import builder from '../builder';

export const Format = builder.objectRef<IFormat>('Format');

Format.implement({
  fields: (t) => ({
    id: t.exposeID('id'),
    length: t.exposeString('name'),
  })
});

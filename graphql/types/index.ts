export interface IPartialTalk {
  id: string;
  type: 'TALK';
  formats: string;
}
export interface ITalk {
  type: 'TALK';
  id: string;
  title: string;
  level: string;
  abstract: string;
  categories: string;
  formats: string;
  speakers: string[];
}

export interface ISecret {
  id: string;
  type: 'SECRET';
  formats: string;
  title: string;
  abstract: string;
  speakers: string[];
}

export interface IWelcom {
  type: 'WELCOM';
  id: string;
  title: string;
}

export interface IKeynote {
  type: 'KEYNOTE';
  id: string;
  title: string;
}

export interface IMeal {
  type: 'MEAL';
  id: string;
  title: string;
}

export interface IEmptySlot {
  type: 'EMPTY';
  id: string;
  title: string;
}

export interface IPause {
  type: 'PAUSE';
  id: string;
  title: string;
}

export interface ICategorie {
  id: string;
  name: string;
  description: string;
}

export interface IFormat {
  id: string;
  name: string;
}

export interface ISpeaker {
  uid: string;
  displayName: string;
  bio: string;
  speakerReferences: string;
  company: string;
  photoURL: string;
  twitter: string;
  github: string;
}

export interface IEvent {
  name: string;
  address: string;
  conferenceDates: {
    start: string;
    end: string;
  };
  categories: ICategorie[];
  formats: IFormat[];
  talks: ITalk[];
  speakers: ISpeaker[];
}

export interface ITrack {
  id: string;
  name: string;
}

export interface ISchedule {
  id: string;
  label: string;
  slots: Array<ITalk | CommonSlot>;
}

export interface IPartialSchedule {
  id: string;
  label: string;
  slots: Array<IPartialTalk | CommonSlot>;
}

export type CommonSlot =
  | IMeal
  | IPause
  | IEmptySlot
  | IKeynote
  | IWelcom
  | ISecret;

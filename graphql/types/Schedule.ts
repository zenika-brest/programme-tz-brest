import { ISchedule } from '.';
import builder from '../builder';
import { Slot } from './Slot';

export const Schedule = builder.objectRef<ISchedule>('Schedule');

Schedule.implement({
  fields: (t) => ({
    id: t.exposeID('id'),
    label: t.exposeString('label'),
    slots: t.expose('slots', { type: [Slot] })
  })
});

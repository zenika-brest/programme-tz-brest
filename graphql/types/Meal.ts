import { IMeal } from '.';
import builder from '../builder';

export const Meal = builder.objectRef<IMeal>('Meal');

Meal.implement({
  fields: (t) => ({
    id: t.exposeID('id'),
    title: t.exposeString('title')
  })
});

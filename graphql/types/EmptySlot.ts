import { IEmptySlot } from '.';
import builder from '../builder';

export const EmptySlot = builder.objectRef<IEmptySlot>('EmptySlot');

EmptySlot.implement({
  fields: (t) => ({
    id: t.exposeID('id'),
    title: t.exposeString('title')
  })
});

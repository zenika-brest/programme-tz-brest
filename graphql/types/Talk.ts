import { ITalk } from '.';
import builder from '../builder';
import { Categorie } from './Categorie';
import { Format } from './Format';
import { Speaker } from './Speaker';

export const Talk = builder.objectRef<ITalk>('Talk');

Talk.implement({
  fields: (t) => ({
    id: t.exposeID('id'),
    title: t.exposeString('title'),
    format: t.field({
      type: Format,
      nullable: true,
      resolve: ({ formats: formatId }, _args, { event: { formats } }) =>
        formats.find(({ id }) => id === formatId)
    }),
    abstract: t.exposeString('abstract'),
    speakers: t.field({
      type: [Speaker],
      resolve: ({ speakers: speakerIds }, _args, { event: { speakers } }) =>
        speakers.filter((speaker) => speakerIds.includes(speaker.uid))
    }),
    categorie: t.field({
      type: Categorie,
      nullable: true,
      resolve: (
        { categories: categorieId },
        _args,
        { event: { categories } }
      ) => categories.find((categorie) => categorieId === categorie.id)
    })
  })
});

builder.queryField('talk', (t) =>
  t.field({
    type: Talk,
    args: {
      id: t.arg({
        type: 'String',
        required: true
      })
    },
    resolve: async (_root, { id }, { event: { talks } }) => {
      const talk = talks.find((talk) => talk.id === id);
      if (!talk) throw new Error(`talk ${id} not found`);
      return talk;
    }
  })
);

builder.queryField('talks', (t) =>
  t.field({
    type: [Talk],
    resolve: (_root, _args, { event: { talks } }) => talks
  })
);

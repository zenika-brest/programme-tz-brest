import { IKeynote } from '.';
import builder from '../builder';

export const Keynote = builder.objectRef<IKeynote>('Keynote');

Keynote.implement({
  fields: (t) => ({
    id: t.exposeID('id'),
    title: t.exposeString('title')
  })
});

import { ISpeaker } from '.';
import builder from '../builder';

export const Speaker = builder.objectRef<ISpeaker>('Speaker');

Speaker.implement({
  fields: (t) => ({
    id: t.exposeID('uid'),
    name: t.exposeString('displayName'),
    avatar: t.exposeString('photoURL')
  })
});

import { IWelcom } from '.';
import builder from '../builder';

export const Welcom = builder.objectRef<IWelcom>('Welcom');

Welcom.implement({
  fields: (t) => ({
    id: t.exposeID('id'),
    title: t.exposeString('title')
  })
});

import { IPause } from '.';
import builder from '../builder';

export const Pause = builder.objectRef<IPause>('Pause');

Pause.implement({
  fields: (t) => ({
    id: t.exposeID('id'),
    title: t.exposeString('title')
  })
});

import { ISecret } from '.';
import builder from '../builder';
import { Format } from './Format';
import { Speaker } from './Speaker';

export const Secret = builder.objectRef<ISecret>('Secret');

Secret.implement({
  fields: (t) => ({
    id: t.exposeID('id'),
    title: t.exposeString('title'),
    abstract: t.exposeString('abstract'),
    format: t.field({
      type: Format,
      nullable: true,
      resolve: ({ formats: formatId }, _args, { event: { formats } }) =>
        formats.find(({ id }) => id === formatId)
    }),
    speakers: t.field({
      type: [Speaker],
      resolve: ({ speakers: speakerIds }, _args, { event: { speakers } }) =>
        speakers.filter((speaker) => speakerIds.includes(speaker.uid))
    })
  })
});

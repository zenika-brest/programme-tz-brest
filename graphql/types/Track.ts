import { ITrack } from '.';
import builder from '../builder';

export const Track = builder.objectRef<ITrack>('Track');

Track.implement({
  fields: (t) => ({
    id: t.exposeID('id'),
    name: t.exposeString('name')
  })
});

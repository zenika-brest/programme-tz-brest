import builder from './builder';

import './types/Event';
import './types/Slot';
import './types/Talk';
import './types/Track';

export default builder.toSchema();

import {
  IKeynote,
  IMeal,
  IPartialSchedule,
  IPartialTalk,
  IPause,
  ISecret,
  IWelcom
} from '../types';

let scheduleId = 0;
let slotId = 0;

export const schedules: IPartialSchedule[] = [
  {
    id: `schedule-${++scheduleId}`,
    label: '9h00 - 9h15',
    slots: [
      {
        id: `pause-${++slotId}`,
        title: 'Accueil',
        type: 'WELCOM'
      } as IWelcom
    ]
  },
  {
    id: `schedule-${++scheduleId}`,
    label: '9h15 - 10h00',
    slots: [
      {
        id: `keynote-${++slotId}`,
        title: 'Keynote',
        type: 'KEYNOTE'
      } as IKeynote
    ]
  },
  {
    id: `schedule-${++scheduleId}`,
    label: '10h00 - 10h20',
    slots: [
      {
        id: `pause-${++slotId}`,
        title: 'Pause',
        type: 'PAUSE'
      } as IPause
    ]
  },
  {
    id: `schedule-${++scheduleId}`,
    label: '10h20 - 10h40',
    slots: [
      {
        id: 'dxeXpnptF9jcPjrLBfP2',
        type: 'TALK',
        formats: 'f1fc16b6-9ff8-5c65-b865-ba6a8434800a'
      } as IPartialTalk,
      {
        id: '70bVSRtpsqGMLiTXEOCv',
        type: 'TALK',
        formats: 'f1fc16b6-9ff8-5c65-b865-ba6a8434800a'
      } as IPartialTalk
    ]
  },
  {
    id: `schedule-${++scheduleId}`,
    label: '10h40 - 11h15',
    slots: [
      {
        id: 'Rzy51rz4u54BlNJYrErI',
        type: 'TALK',
        formats: 'e0a2672b-116a-5937-875b-5021e3fca30e'
      } as IPartialTalk,
      {
        id: 'TwmEstapXxkiaoZhcH8d',
        type: 'TALK',
        formats: 'e0a2672b-116a-5937-875b-5021e3fca30e'
      } as IPartialTalk
    ]
  },
  {
    id: `schedule-${++scheduleId}`,
    label: '11h15 - 11h25',
    slots: [
      {
        id: `pause-${++slotId}`,
        title: 'Pause',
        type: 'PAUSE'
      } as IPause
    ]
  },
  {
    id: `schedule-${++scheduleId}`,
    label: '11h25 - 12h00',
    slots: [
      {
        id: 'BEcUcoIF1QCyYLr1bqwE',
        type: 'TALK',
        formats: 'e0a2672b-116a-5937-875b-5021e3fca30e'
      } as IPartialTalk
    ]
  },
  {
    id: `schedule-${++scheduleId}`,
    label: '12h00 - 12h20',
    slots: [
      {
        id: `secret-${++slotId}`,
        type: 'SECRET',
        title: 'Secret santa 🎄',
        abstract: `Tirez au sort et découvrez à qui vous allez offrir un cadeau ! Nous échangerons nos cadeaux !
Quelques idées pour trouver un cadeau sympa :
- enquêtez (discrètement...) sur ce qu'aime la personne,
- ou réfléchissez à quelque chose que vous aimez et que vous aimeriez partager/faire découvrir`,
        formats: 'f1fc16b6-9ff8-5c65-b865-ba6a8434800a',
        speakers: [
          'PISLohru8Vcnb11Fhk9DZL5iYSy1',
          'eYgTTnbNMWUhkGN6CvnCUzBwF683'
        ]
      } as ISecret
    ]
  },
  {
    id: `schedule-${++scheduleId}`,
    label: '12h20 - 14h00',
    slots: [
      {
        id: `meal-${++slotId}`,
        title: 'Repas du midi',
        type: 'MEAL'
      } as IMeal
    ]
  },
  {
    id: `schedule-${++scheduleId}`,
    label: '14h00 - 14h35',
    slots: [
      {
        id: 'oMbUuE2U9mpye6vz5WDp',
        type: 'TALK',
        formats: 'e0a2672b-116a-5937-875b-5021e3fca30e'
      } as IPartialTalk,
      {
        id: 'VUwBEI5QjFXwpcQHQnvL',
        type: 'TALK',
        formats: 'e0a2672b-116a-5937-875b-5021e3fca30e'
      } as IPartialTalk
    ]
  },
  {
    id: `schedule-${++scheduleId}`,
    label: '14h35 - 14h55',
    slots: [
      {
        id: '7wRjPBAjFyA97Vi2pMef',
        type: 'TALK',
        formats: 'f1fc16b6-9ff8-5c65-b865-ba6a8434800a'
      } as IPartialTalk,
      {
        id: '6lzKguZyGUwiAupAJgWS',
        type: 'TALK',
        formats: 'f1fc16b6-9ff8-5c65-b865-ba6a8434800a'
      } as IPartialTalk
    ]
  },
  {
    id: `schedule-${++scheduleId}`,
    label: '14h55 - 15h05',
    slots: [
      {
        id: `pause-${++slotId}`,
        title: 'Pause',
        type: 'PAUSE'
      } as IPause
    ]
  },
  {
    id: `schedule-${++scheduleId}`,
    label: '15h05 - 15h40',
    slots: [
      {
        id: 'm7HXrGmxylFczxNEmMVz',
        type: 'TALK',
        formats: 'e0a2672b-116a-5937-875b-5021e3fca30e'
      } as IPartialTalk
    ]
  },
  {
    id: `schedule-${++scheduleId}`,
    label: '15h40 - 16h15',
    slots: [
      {
        id: 'a0fI6Szw824s2amPbnLP',
        type: 'TALK',
        formats: 'e0a2672b-116a-5937-875b-5021e3fca30e'
      } as IPartialTalk,
      {
        id: 'mT92eK4V6tvddMrKlOil',
        type: 'TALK',
        formats: 'e0a2672b-116a-5937-875b-5021e3fca30e'
      } as IPartialTalk
    ]
  },
  {
    id: `schedule-${++scheduleId}`,
    label: '16h15 - 16h25',
    slots: [
      {
        id: `pause-${++slotId}`,
        title: 'Pause',
        type: 'PAUSE'
      } as IPause
    ]
  },
  {
    id: `schedule-${++scheduleId}`,
    label: '16h25 - 17h00',
    slots: [
      {
        id: 'GMNv44jh9JP9Ut8jBzaq',
        type: 'TALK',
        formats: 'e0a2672b-116a-5937-875b-5021e3fca30e'
      } as IPartialTalk,
      {
        id: 'maOmhFUueoqePh4KVNXU',
        type: 'TALK',
        formats: 'e0a2672b-116a-5937-875b-5021e3fca30e'
      } as IPartialTalk
    ]
  },
  {
    id: `schedule-${++scheduleId}`,
    label: '17h00 - 17h20',
    slots: [
      {
        id: 'y8tbCbw7KFUZFB6DLyJQ',
        type: 'TALK',
        formats: 'f1fc16b6-9ff8-5c65-b865-ba6a8434800a'
      } as IPartialTalk,
      {
        id: 'KugLgIbYTGqJapdOF0DJ',
        type: 'TALK',
        formats: 'f1fc16b6-9ff8-5c65-b865-ba6a8434800a'
      } as IPartialTalk
    ]
  },
  {
    id: `schedule-${++scheduleId}`,
    label: '17h20 - 18h00',
    slots: [
      {
        id: `pause-${++slotId}`,
        title: 'Keynote',
        type: 'KEYNOTE'
      } as IKeynote
    ]
  }
];

import { ITrack } from '~graphql/types';

let trackId = 0;

export const track1: ITrack = {
  id: `track${++trackId}`,
  name: 'Espace détente'
};

export const track2: ITrack = {
  id: `track${++trackId}`,
  name: 'Hamilton'
};

export const tracks = [track1, track2];

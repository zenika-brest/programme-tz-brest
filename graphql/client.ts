import { ApolloClient, InMemoryCache } from '@apollo/client';
import { SchemaLink } from '@apollo/client/link/schema';
import context from '~graphql/context';
import schema from '~graphql/schema';

const client = new ApolloClient({
  ssrMode: true,
  link: new SchemaLink({
    schema,
    context
  }),
  cache: new InMemoryCache()
});

export default client;

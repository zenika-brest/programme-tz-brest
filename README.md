# Getting Started

First, create file `.env.local`:

```bash
# .env.local
ENVT_ID=
API_KEY=
```

And run the development server:

```bash
pnpm install
pnpm dev
```

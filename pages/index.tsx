import { MoonIcon, SunIcon } from '@chakra-ui/icons';
import {
  Button,
  ColorModeScript,
  Container,
  Flex,
  Heading,
  Text,
  useColorMode
} from '@chakra-ui/react';
import { GetStaticProps, NextPage } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import { Table } from '~components/Table';
import {
  ProgrammeTableDocument,
  ProgrammeTableQuery,
  ProgrammeTableQueryResult
} from '~generated/operations';
import client from '~graphql/client';
import theme from '~theme/theme';
import logo from '../public/logozenika.svg';

export const getStaticProps: GetStaticProps = async () => {
  const { data } = await client.query<ProgrammeTableQueryResult>({
    query: ProgrammeTableDocument
  });

  return {
    props: { data }
  };
};

type ProgrammeTableProps = {
  data: ProgrammeTableQuery;
};

const HomePage: NextPage<ProgrammeTableProps> = ({ data }) => {
  const { colorMode, toggleColorMode } = useColorMode();

  return (
    <>
      <ColorModeScript initialColorMode={theme.config.initialColorMode} />
      <Head>
        <title>Programme TZ Brest</title>
        <meta name="description" content="TZ Brest 9 décembre 2022" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Container maxW="6xl">
        <Flex justifyContent="space-between" alignItems="center">
          <Heading mb={4}>TZ Brest 9 décembre 2022</Heading>
          <Button onClick={toggleColorMode} mr={2}>
            {colorMode === 'dark' ? <SunIcon /> : <MoonIcon />}
          </Button>
        </Flex>

        <Flex justifyContent="space-between">
          <Text fontSize="xl">Programme de la journée</Text>
          <Image src={logo} width={100} alt="" />
        </Flex>

        <Table data={data} />
      </Container>
    </>
  );
};

export default HomePage;

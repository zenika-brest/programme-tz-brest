import axios from 'axios';
import { NextApiRequest, NextApiResponse } from 'next';
import { match, P } from 'ts-pattern';
import { IEvent } from '~graphql/types';

export type EventState = 'submitted' | 'accepted' | 'backup' | 'rejected';

export const fetchEvent = async (
  id: string,
  state?: EventState
): Promise<IEvent> => {
  const key = process.env.API_KEY;
  if (!key) throw Error('process.env.API_KEY not found');

  const response = await axios.get<IEvent>(
    `https://conference-hall.io/api/v1/event/${id}`,
    {
      params: {
        key,
        state
      }
    }
  );
  return response.data;
};

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const id = match(req.query.id)
    .with(P.string, (id) => id)
    .with(P.array(P.string), (ids) => ids.join(', '))
    .with(undefined, () => 'undefined')
    .exhaustive();
  res.status(200).json(await fetchEvent(id));
};

export default handler;

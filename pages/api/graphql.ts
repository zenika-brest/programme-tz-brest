import { createYoga } from 'graphql-yoga';
import { NextApiRequest, NextApiResponse } from 'next';
import context from '~graphql/context';
import schema from '~graphql/schema';

export default createYoga<{
  req: NextApiRequest;
  res: NextApiResponse;
}>({
  graphqlEndpoint: '/api/graphql',
  schema,
  context
});
